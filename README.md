A demo project that allows:

Towards a free floating management system 分时租赁系统 whose typical usage is communicating
in urban area, the typical usage time is under one hour. 

- add a new vehicle 
- book a vehicle
- cancel a booking
- expire a booking
- start a trip
- end a trip

there are no
- fixed parking places
- don't have to indicate when and where to return the vehicle

Design paradigms 
- focus on atomic unit operations, more complex operations can be composed by these atomic operations
- log powered peripheral modules like notification (email, sms), billing (at the end of trip)


User use case: 
- A user open an app, and the app shows all available vehicles on an app
- The user chooses one vehicle and books it
- The booked vehicle is now only visible from the guy who booked it
- There should be some indicator on the vehicle to show that it is booked
so that the guy who is just near the vehicle know that it is not available
- The user can cancel the booking 
- The booking will not be expired, but he will be charged extra money 
after 15 minutes
- The user comes to the vehicle and unlock it by the network or bluetooth 

Admin use case: 


1. Commands are instructions from user, for example, 
book a vehicle, end a trip
2. Events include periodic reports from the vehicle 
(position updated, speed, engine started, fuel gauge, refill)
or events related to the trip (trip started, trip ended)


TODO (by priories):
- add vehicle actor 
- move to actors typed 
- add trip manager and trip actors
- add billing cluster 
- add data process / Spark integration


 