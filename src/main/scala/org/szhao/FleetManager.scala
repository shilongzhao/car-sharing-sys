//#full-example
package org.szhao

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import org.szhao.core.vehicle.VehicleManager
import org.szhao.core.vehicle.VehicleManager.Register

import scala.concurrent.ExecutionContextExecutor

//#main-class
object FleetManager extends App {

  val config = ConfigFactory.load()
  implicit val system: ActorSystem = ActorSystem("fleet")
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val vehicleManager = system.actorOf(Props(new VehicleManager), "vehicles")
  //#main-send-messages
}
//#main-class
//#full-example
