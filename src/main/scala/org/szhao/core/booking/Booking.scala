package org.szhao.core.booking

import akka.actor.{ActorLogging, FSM}
import org.szhao.core.booking.Booking.{BData, BState}
import org.szhao.core.vehicle.Vehicle

/**
 * @author zsh
 *         created 2020/Nov/28
 *
 * booking FSM
 */
object Booking {
  trait BState
  trait BData
}

class Booking(vehicle: Vehicle) extends FSM[BState, BData] with ActorLogging {

}
