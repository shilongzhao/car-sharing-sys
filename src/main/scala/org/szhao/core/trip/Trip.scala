package org.szhao.core.trip

import akka.actor.{ActorLogging, ActorRef, FSM}
import org.szhao.core.trip.Trip.{TData, TState}


/**
 * @author zsh
 *         created 2020/Nov/28
 */
object Trip {
  sealed trait TState
  sealed trait TData
}
class Trip(vehicle: ActorRef) extends FSM[TState, TData] with ActorLogging {

}
