package org.szhao.core.vehicle

import java.time.LocalDateTime

import akka.actor.{ActorLogging, ActorRef, FSM}
import org.szhao.core.vehicle.Vehicle._

import scala.language.postfixOps

/**
 * @author zsh
 * created 22/Sep/2020
 *
 * vehicle status has nothing to do with booking status or trip status
 *
 * for a vehicle, its statuses are only, for example, available or not available,
 * or partial available to admins (during maintenance), we do not consider any trip or booking status here -- they
 * are decoupled
 *
 */
object Vehicle {
  // FSM state and data
  type ReplyTo = ActorRef
  sealed trait VState
  case object Offline extends VState
  case object Online extends VState
  case object Taken extends VState

  case class VData(logs: List[VLog])

  sealed trait VEvent
  // received events
  case class UpdatePosition(position: Position, replyTo: ReplyTo) extends VEvent

  case class Connect(replyTo: ReplyTo) extends VEvent// offline -> online
  case class Disconnect(replyTo: ReplyTo) extends VEvent// online -> offline
  case class Take(replyTo: ReplyTo) extends VEvent// online -> taken
  case class Release(replyTo: ReplyTo) extends VEvent // taken -> online

  case class VLog(datetime: LocalDateTime, event: VEvent)

  // sent events
  sealed trait VResponse

  // ...

  case class Position(latitude: Double, longitude: Double, altitude:Double = 0);

}


class Vehicle(id: String) extends ActorLogging with FSM[VState, VData] {

  var position:Option[Position] = None
  var trip: Option[ActorRef] = None
  var booking: Option[ActorRef] = None
  var embeddedChip: Option[ActorRef] = None
  var notifier: Option[ActorRef] = None

  startWith(Offline, VData(Nil))

  onTransition {
    case x -> y =>
      log.info("state transition: {} --> {}", x, y)
  }

  when(Offline) {
    case Event(Connect, stateData) =>
      goto(Online) using stateData
  }

  when(Online) {
    case Event(Take(replyTo), stateData) =>
      replyTo ! VTaken
      goto(Taken) using stateData
    case Event(Disconnect(replyTo), stateData) =>
      replyTo ! VDisconnected
      goto(Offline) using stateData
  }

  when(Taken) {
    case Event(Release(replyTo), stateData) =>
      replyTo ! VReleased
      goto(Online) using stateData
  }

  whenUnhandled {
    case Event(UpdatePosition(pos, replyTo), s) =>
      replyTo ! Vehicle
      stay()
    case Event(Vehicle.GetVInfo(replyTo), s) =>
      replyTo ! VInfo(id, stateName, stateData)
      stay()
    case Event(e, s) =>
      log.warning("received unknown request {} in state {}/{}", e, stateName, s)
      stay()
  }

  initialize()
  log.info("vehicle actor {} starts up OK", id)
}
