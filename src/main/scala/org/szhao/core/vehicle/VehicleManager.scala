package org.szhao.core.vehicle

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import akka.util.Timeout
import org.szhao.core.vehicle.VehicleManager._

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
/**
 * @author zsh
 * created 22/Sep/2020
 */
object VehicleManager {
  type ReplyTo = ActorRef
  // received events
  case class Register(id: String, replyTo: ReplyTo)
  case class Disconnect(id: String, replyTo: ReplyTo)

  case class GetVInfo(id: String, replyTo: ActorRef)
  case class GetVInfoAll(replyTo: ActorRef)

  // sent events
  sealed trait VMResponse
  case class VInfoAll(value: Vector[Vehicle.VInfo]) extends VMResponse
  case object VehicleCreated extends VMResponse
  case object VehicleExists extends VMResponse
  case object VehicleNotFound extends VMResponse
}

class VehicleManager extends Actor with ActorLogging {

  implicit val ec: ExecutionContext = context.dispatcher
  implicit val timeout: Timeout = 1 seconds
  override def receive: Receive = {
    case Register(id, replyTo) =>
      def create(): Unit = {
        val vehicleActor = context.actorOf(Props(new Vehicle(id)), id)
        replyTo ! VehicleCreated
      }
      context.child(id).fold(create())(_ => sender() ! VehicleExists)

    case Disconnect(id, replyTo) =>
      def notFound(): Unit = replyTo ! VehicleNotFound
      def disable(childActor: ActorRef): Unit = childActor ! Vehicle.Disconnect(replyTo)
      context.child(id).fold(notFound())(disable)

    case Terminated(actorRef) =>
      log.info("actor is dead {}", actorRef)

    case t @ GetVInfo(id, replyTo) =>
      log.info("get state data {}", id)
      def notFound(): Unit = replyTo ! VehicleNotFound
      def stateInfo(childActor: ActorRef): Unit = childActor ! t

      context.child(id).fold(notFound())(stateInfo)

    case GetVInfoAll(replyTo) =>
      import akka.pattern.ask
      import akka.pattern.pipe

      def getVInfoAll = context.children.map { child =>
        self.ask(Vehicle.GetVInfo(self)).mapTo[Option[Vehicle.VInfo]]
      }
      def convertToEvents(f: Future[Iterable[Option[Vehicle.VInfo]]]) =
        f.map(_.flatten).map(l => VInfoAll(l.toVector))

      pipe(convertToEvents(Future.sequence(getVInfoAll))) to replyTo
    case t =>
      log.warning("unknown message format {}", t)
  }

  log.info("vehicle manager starts up")
}
