package org.szhao.core.vehicle

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.must.Matchers

/**
 * @author zsh
 * created 02/Oct/2020
 */
class VehicleSpec extends TestKit(ActorSystem("testVehicles"))
  with AnyWordSpecLike
  with Matchers
  with ImplicitSender
  with StopSystemAfterAll {

}
